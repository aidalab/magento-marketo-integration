
function runSync(url) {
    //new Ajax.Request('/hooshmarketing/index/syncCategoryScores');
    new Ajax.Request(url);


    if(typeof(billing) != "undefined") {
        var oldNextStep = billing.nextStep;

        billing.nextStep = function (transport) {
            new Ajax.Request(url);
            oldNextStep(transport);
        };

        billing.onSave = billing.nextStep.bindAsEventListener(billing);
    }
}


