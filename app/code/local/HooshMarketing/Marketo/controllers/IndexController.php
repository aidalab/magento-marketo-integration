<?php

class HooshMarketing_Marketo_indexController extends Mage_Core_Controller_Front_Action
{
    const PRODUCT_IMAGE_PARAM = "product_id";

    /**
     * This function generate image of product on custom page
     */
    public function getImageAction()
    {
        $productId = $this->getRequest()->getParam(self::PRODUCT_IMAGE_PARAM);
        /** @var Mage_Catalog_Model_Product $product */
        $product = Mage::getModel("catalog/product")->load($productId);

        if (is_integer((int)$productId) && !empty($product) && $product->getId() != null) {
            header("Content-Type: image");
            echo file_get_contents($product->getImageUrl());
        } else {
            echo "This product doesn`t exist!";
        }

    }
    /* @return HooshMarketing_Marketo_Helper_Data */
    protected function _getHelper() {
        return Mage::helper("hoosh_marketo");
    }

    /**
     * Syncing Billing Address Without Saving it
     * @return bool
     */
    public function indexAction() {
        /** @var Mage_Checkout_Model_Session $_checkoutSession */
        $_checkoutSession = Mage::getSingleton("checkout/session");
        $_billingAddress  = $_checkoutSession->getQuote()->getBillingAddress();
        /** @var array $params -> Post from checkout onepage */
        $params = $this->getRequest()->getPost();

        $_billingAddress->addData($params);
        //Trying to synchronize Data
        $this->_getLeadModel()->prepareLeadToSync(
            $this->_getHelper()->getCompanyParamToSync(),
            array("billing_address" => $_billingAddress)
        );

        return true;
    }

    /* @return HooshMarketing_Marketo_Model_Lead */
    protected function _getLeadModel() {
        return Mage::getSingleton("hoosh_marketo/lead");
    }
    /**
     * @param Mage_Core_Model_Abstract $model
     * @return HooshMarketing_Marketo_Model_Opportunity
     */
    protected function _getOpportunityModel($model = null) {
        return (!empty($model)) ? Mage::getModel("hoosh_marketo/opportunity") : Mage::getSingleton("hoosh_marketo/opportunity");
    }

    public function getLeadAction()
    {
        $lead = $this->_getLeadModel()->getLoadedByCookie();
        var_dump($lead->getData());
    }

    public function getOpportunityAction() {
        $lead = $this->_getLeadModel()->getLoadedByCookie();
        $collection = $this
            ->_getOpportunityModel()
            ->getCollection()
            ->addAttributeToSelect("*")
            ->addFieldToFilter("parent_id", $lead->getId());
        echo "<h1>Opportuniry count: ". $collection->count() . "</h1>";
        /** @var HooshMarketing_Marketo_Model_Opportunity $opportunity */
        foreach($collection as $opportunity) {
            var_dump($opportunity->getData());
        }
    }

    public function getLastViewedAction() {
        $_id = $this->getRequest()->getParam("id"); 

        $_result = Mage::getSingleton("hoosh_marketo/opportunity")->getInfoFromProductId($_id, 1); 
        var_dump($_result); 
    }

    /* synchronize cron opportunity and lead */
    public function syncAction() {
        /** @var HooshMarketing_Marketo_Model_Cron $cron */
        $cron = Mage::getSingleton("hoosh_marketo/cron");
        $cron->syncLeadAndOpportunityData();
    }

    public function syncInactivityAction() {
        /** @var HooshMarketing_Marketo_Model_Cron $cron */
        $cron = Mage::getSingleton("hoosh_marketo/cron");
        $cron->syncInactivityLeads();
    }

    public function getCartAction()
    {
        $quoteId = $this->getRequest()->getParam('cartID');
        $mktTrk = Mage::getModel('core/cookie')->get('_mkto_trk');
        if (!empty($mktTrk) && !empty($quoteId)) {
            /** @var $quote Mage_Sales_Model_Quote */
            $quote = Mage::getModel('sales/quote')->load((int)$quoteId);
            if (!$quote->isEmpty()) {
                /** @var $currentQuote Mage_Sales_Model_Quote */
                if (Mage::getSingleton('checkout/session')->getQuoteId()) {
                    if (Mage::getSingleton('checkout/session')->getQuoteId() != $quoteId) {
                        $currentQuote = Mage::getModel('sales/quote')->load(Mage::getSingleton('checkout/session')->getQuoteId());
                        $currentQuote->merge($quote);
                        $currentQuote->collectTotals()->save();
                    }
                } else {
                    $quote->setIsActive(true);
                    $quote->collectTotals()->save();
                    Mage::getSingleton('checkout/session')->setQuoteId($quoteId);
                }

                $this->_redirectSuccess(Mage::getUrl('checkout/cart/index'));
                return;
            }
        }

        $this->_redirectError(Mage::getUrl('cart-not-found'));
        return;
    }
}