<?php
class HooshMarketing_Marketo_Model_Category extends Mage_Core_Model_Abstract
{
    /* scoring constants */
    const CATEGORY_STEP = "score_viewed";
    const CHECKOUT_CART_STEP = "score_add_to_cart";
    const ORDER_CART_STEP = "score_purchased";
    const EXTERNAL_URL_STEP = "score_external_url";

    /* cache scope constants */
    const PRIORITIZING_CATEGORY = "prioritize_category";

    private $__rootIgnoreCategories = array(1,2);
    public $lastCurrentStore;
    public $calculatingLead;
    protected $_sorter = 0;
    protected $_clearedCollectionKeys = array(
        Zend_Db_Select::WHERE => "where",
        Zend_Db_Select::ORDER => "order",
        Zend_Db_Select::LIMIT_COUNT => "limit"
    );
    protected $_clearedCollectionData = array();

    public function getIgnoreCategories() {
        return $this->__rootIgnoreCategories;
    }

    protected function _construct() {
        $this->_init("hoosh_marketo/category");
    }

    public function getAllCategoriesNamesAndIds() {
        $read = Mage::getModel('core/resource')->getConnection('core_read');
        $query = $read->select()->from(
            array(Mage::getSingleton("core/resource")->getTableName('catalog_category_entity')),
            array('modified_names'=>'MARKETOSPLIT(path)', 'id'=>'entity_id')
        );

        return $read->fetchAll($query);
    }

    public function getConfig($field, $unserialize = false) {
        return ($unserialize) ? unserialize(Mage::getStoreConfig("marketo_config/category_settings/$field")): Mage::getStoreConfig("marketo_config/category_settings/$field");
    }

    public function getMappCats() {
        $mappCats = array();

        foreach((array)$this->getConfig("map_categories", true) as $_field) {
            if(isset($_field["marketo_category_field"]) && isset($_field["magento_category"])) {
                $mappCats[$_field["marketo_category_field"]] = $_field["magento_category"];
            }
        }

        return $mappCats;
    }

    protected function _stripPlus($config) {
        return preg_replace("/[\+]/",  "", $config);
    }

    protected function _calculateStepValue($stepType) {
        /* If config exists */
        if($_stepValue = $this->getConfig($stepType)) {
            return $this->_calculateStepValue($this->_stripPlus($_stepValue));
        }

        if(floatval($stepType)) { //if it`s digit with + or another symbols
            return floatval($stepType);
        }

        return 1;
    }

    public function _onlyIds(array $categories) {
        foreach($categories as $key=>$value) {
            $categories[$this->_onlyParam(2, $key)] = $value;
            unset($categories[$key]);
        }

        return $categories;
    }
    /* get only category name or category id from value
     * 1 - letters
     * 2 - id
     */
    public function _onlyParam($paramNumber, $string) {
        return preg_replace("/^(.*)_#(\d+)$/", "$".$paramNumber, $string);
    }

    public function score(Mage_Catalog_Model_Category $category = null, Mage_Catalog_Model_Product $product = null, $stepType) {
        $_mappedCategories = $this->getMappCats();
        if(!empty($product)) { // if we have product - than we could have few categories
            $productCatPath = array();
            $catCollection = ($product->getCategoryCollection() == null) ? Mage::getModel("catalog/product")->load($product->getId())->getCategoryCollection() :  $product->getCategoryCollection();

            foreach($catCollection as $category) {
                foreach(explode("/", $category->getPath()) as $id) {
                    $productCatPath[$id] = $id;
                }
            }

            $this->_addScores(
                array_intersect_key(
                    $this->_onlyIds($_mappedCategories),
                    $productCatPath
                    ),
                $this->_calculateStepValue($stepType));

        } elseif(!empty($category)) { // we have only one category
            $_ids = explode("/", $category->getPath());

            $this->_addScores(
                array_intersect_key(
                    $this->_onlyIds($_mappedCategories),
                    array_flip(array_diff($_ids, $this->getIgnoreCategories()))),
                $this->_calculateStepValue($stepType));

        }
    }

    protected function _ignoreNewTopCategory() {
        $marketoModel = Mage::getSingleton("hoosh_marketo/marketo");
        $prioritizeData = $marketoModel->getCacheScope(self::PRIORITIZING_CATEGORY);

        if(isset($prioritizeData[0])) {
            $marketoModel->flushCacheScope(self::PRIORITIZING_CATEGORY);
            return false;
        } else {
            $marketoModel->setCacheScope(self::PRIORITIZING_CATEGORY, array(true));
            return true;
        }
    }

    /* get threshold for making category - top */
    protected function _getThreshold() {
        return ($this->getConfig("threshold") == null) ? 1: $this->getConfig("threshold");
    }

    /* get count of slashes  - 1 */
    protected function _getNestingLvl($categoryPath) {
        return substr_count($categoryPath, "/") - 1; // /Fashiom = 0; /Fashion/Shoes = 1
    }

    /* @nesting - slash qty for example /Fashion/Shoes = 2 */

    public function calculate($lead) {
        if(!is_array($lead)) return false;
        $maxValue = 0;
        $mappCats = array_flip($this->getMappCats());

        $toCompare = array_intersect_key($lead, $mappCats);

        foreach($toCompare as $key=>$value) {
            /* if value is bigger then calculated max value and value is bigger then threshiold for this category */
            if($value >= $maxValue && $value >= $this->_getThreshold() * $this->_getNestingLvl($mappCats[$key])) {
                    /* preparing data to next iteration */
                    $maxValue = $value;
                    $topCategoryName = $this->_onlyParam(1, $mappCats[$key]);
                    $topCategoryId = $this->_onlyParam(2, $mappCats[$key]);

                    /* setting new data */
                    $toCompare[$this->getConfig("map_top_field")] = $topCategoryName;
                    $this->setTopCategoryId($topCategoryId);
            }
        }

        Mage::getSingleton("hoosh_marketo/marketo")->setCacheScope("lead", $toCompare);

        return $toCompare;
    }

    public function setTopCategoryId($id) {
        if(Mage::registry("top_category")) {
            Mage::unregister("top_category");
        }
        
        Mage::register("top_category", $id);
    }

    public function customScore(Zend_Controller_Request_Abstract $request) {
        $categoryVariables = $request->getParams();
        $categoryIds = array();

        $mappedCategoryPathces = array_flip($this->getMappCats());

        foreach($categoryVariables as $variable => $score) {
            if(isset($mappedCategoryPathces[$variable])) {
                $categoryIds[$this->_onlyParam(2, $mappedCategoryPathces[$variable])] = $score;
            }
        }

        if(empty($categoryIds)) return false;

        $_collection = Mage::getSingleton("catalog/category")
            ->getCollection()
            ->addFieldToFilter("entity_id", array_keys($categoryIds));

        foreach($_collection as $category) {
            $this->score($category, null,
                ((bool)$categoryIds[$category->getId()]) ? $categoryIds[$category->getId()] : self::EXTERNAL_URL_STEP
            );
        }

        return true;
    }

    public function setStoreView($calculatingLeadData) {
        if(empty($this->lastCurrentStore)) {
            $this->lastCurrentStore = Mage::app()->getStore();
        }

        /* TODO if template of chosen category is null*/

        if(isset($calculatingLeadData[$this->getConfig("map_top_field")]) && isset($calculatingLeadData[$this->_getTemplateConfig("map_top_field")])) {
            $this->_setNewStore($calculatingLeadData);
        }
    }

    protected function _setNewStore($calculatingLeadData) {
        $currentStoreCode = $this->_getThemeName($calculatingLeadData[$this->_getTemplateConfig("map_top_field")]);

        if(is_integer((int)Mage::getSingleton("core/store")->load($currentStoreCode, "code")->getId())
            && $this->_getThemeName($calculatingLeadData[$this->_getTemplateConfig("map_top_field")]) != NULL) {

            Mage::app()->setCurrentStore($this->_getThemeName($calculatingLeadData[$this->_getTemplateConfig("map_top_field")]));
            $this->lastCurrentStore->setId(Mage::app()->getStore()->getId());
        }
    }

    protected function _getTemplateConfig($field, $unserialize = false) {
        return ($unserialize) ? unserialize(Mage::getStoreConfig("marketo_config/template_switching/$field")): Mage::getStoreConfig("marketo_config/template_switching/$field");
    }

    protected function _getThemeName($topField) {
        $templates = $this->_getTemplateConfig("map_categories", 1);

        foreach($templates as $template) {
            if(empty($template)) continue;
            if($this->_onlyParam(1, $template["marketo_variable"]) == $topField) {
                return $template["template"];
            }
        }
    }

    protected function _addScores(array $toIncreaseScore, $step) {
        $cache = Mage::getSingleton("hoosh_marketo/marketo")->getCacheScope("lead");

        array_walk($toIncreaseScore, function($marketoKey) use(&$cache, $step) {
            if(!isset($cache[$marketoKey])) {
                $cache[$marketoKey] = $step;
            } else {
                $cache[$marketoKey] += $step;
            }
        });

        Mage::getSingleton("hoosh_marketo/marketo")->setCacheScope("lead", $cache);
    }

    protected function _clearProductCollection(Mage_Catalog_Model_Resource_Product_Collection &$productCollection) {
        foreach(array_keys($this->_clearedCollectionKeys) as $keyToClear) {
            try {
                $this->_clearedCollectionData[$keyToClear] = $productCollection->getSelect()->getPart($keyToClear);
                $productCollection->getSelect()->reset($keyToClear);
            } catch(Exception $e) {
                Mage::helper("hoosh_marketo")->log($e->getMessage());
            }
        }
    }

    public function _restoreProductCollection(Mage_Catalog_Model_Resource_Product_Collection &$productCollection) {
        foreach($this->_clearedCollectionKeys as $key=>$function) {
            try {
                if(is_array($this->_clearedCollectionData[$key]) && $key == "where") {
                    $where = "";
                    foreach($this->_clearedCollectionData[$key] as $keyPart) {
                        $where .= " ".$keyPart;
                    }
                    if(!empty($where)) {
                        $productCollection->getSelect()->{$function}($where);
                    }
                } else {
                    $productCollection->getSelect()->{$function}($this->_clearedCollectionData[$key]);
                }

            } catch(Exception $e) {
                Mage::helper("hoosh_marketo")->log($e->getMessage());
            }
        }
    }

    /* add attribute to sort -> top_category_id, is NULL or number with top category */
    public function sort($productCollection) {
        if(!$topCategoryId = Mage::registry("top_category")) return $productCollection;
        if($productCollection->getFlag("marketo_sorted")) return $productCollection;

        $productCategoryTable = Mage::getSingleton("core/resource")->getTableName("catalog_category_product");
        $this->_clearProductCollection($productCollection);

        try {
            $productCollection
                ->getSelect()
                ->joinLeft(array("cpp"=>$productCategoryTable), "cpp.product_id = e.entity_id AND cpp.category_id=".$topCategoryId, array("top_category_id"=>"cpp.category_id"))
                ->order("cpp.category_id DESC");

            $productCollection->setFlag("marketo_sorted", 1); //to set order only one time
        } catch(Exception $e) {
            Mage::helper("hoosh_marketo")->log($e->getMessage());
        }

        $this->_restoreProductCollection($productCollection);

        return $productCollection;

    }

    public function genereatePathString($breadCrumbPath, $path) {
        foreach($breadCrumbPath as $category) {
            $path .= $category["label"]."/";
        }

        return $path;
    }

    public function addCategoryPath() {
        $path = "/";
        $breadCrumbPath = Mage::helper('catalog')->getBreadcrumbPath();

        if (Mage::registry('current_product')) {
            array_pop($breadCrumbPath);
        }

        if(count($breadCrumbPath) == 0) {
            return false;
        }

        $_path = $this->_stripping($this->genereatePathString($breadCrumbPath, $path));

        try {
            $this->loadByReferer($this->_getCurrentUrlWithoutPort());
            $this->setCategoryPath($_path)
                ->setRefererUrl($this->_getCurrentUrlWithoutPort())
                ->save();
        } catch(Exception $e) {
            Mage::log($e);
        }

        return $_path;
    }

    public function loadByReferer($referer) {
        $this->load($referer, "referer_url");
    }

    protected function _stripping($path) {
        $path = substr($path, 0, -1); //remove last slash
        $path = preg_replace("/\s{2,}/", "", $path);

        return $path;
    }

    protected function _getCurrentUrlWithoutPort()
    {
        $request = Mage::app()->getRequest();
        $url = $request->getScheme() . '://' . $request->getHttpHost() . $request->getServer('REQUEST_URI');
        return $url;
    }

    public function setCategoryPathToQuoteItem(Mage_Sales_Model_Quote_Item $quoteItem) {
        $referer = Mage::helper("core/http")->getHttpReferer();
        $this->loadByReferer($referer);
        $path =  $this->getCategoryPath();
        $quoteItem->setCategoryPath($path);

        return $quoteItem;
    }
}