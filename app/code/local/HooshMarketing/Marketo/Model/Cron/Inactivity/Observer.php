<?php

class Hooshmarketing_Marketo_Model_Cron_Inactivity_Observer extends Mage_Core_Model_Observer
{
    public function logLeadActivity()
    {

        if (!isset($_COOKIE['_mkto_trk'])) {
            return;
        }

        $leadModel = Mage::getModel("hoosh_marketo/marketo");
        $storeId = Mage::app()->getStore()->getStoreId();

        $timestamp = Mage::app()->getLocale()->storeTimeStamp($storeId);
        $time = date('Y-m-d H:i:s', $timestamp);

        $leadCookie = $_COOKIE['_mkto_trk'];
        $lead = $leadModel->load($leadCookie, 'lead_cookie');

        if ($lead->getId()) {
            try {
                $leadModel->load($lead->getId());
                $data = array("last_activity_time" => $time, 'store_id' => $storeId);
                $leadModel->addData($data);
            } catch (Exception $e) {
                Mage::logException($e);
            }
        } else {
            $lead = $leadModel->getLead();

            if (empty($lead->result->leadRecordList->leadRecord->Id)) {
                return false;
            }

            $leadId = $lead->result->leadRecordList->leadRecord->Id;
            $email = $lead->result->leadRecordList->leadRecord->Email;

            if (empty($email)) {
                return false;
            }

            $data = array("lead_id" => $leadId, "lead_cookie" => $leadCookie, 'last_activity_time' => $time, 'store_id' => $storeId);

            try {
                $leadModel->setData($data);
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }
        try {
            $leadModel->save();
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

    public function syncInactiveLeads()
    {
        $allStores = Mage::app()->getStores();
        $leadModel = Mage::getModel("hoosh_marketo/marketo");

        $leadRecordList = array();

        foreach ($allStores as $store => $storeValue) {
            $storeId = Mage::app()->getStore($store)->getId();
            $timestamp = Mage::app()->getLocale()->storeTimeStamp($storeId);

            $collection = $leadModel->getCollection()
                ->addFieldToFilter("store_id", $storeId)
                ->addFieldToFilter("last_activity_time", array(
                        "from" => date('Y-m-d H:i:s', $timestamp - 30 * 60),
                        "to" => date('Y-m-d H:i:s', $timestamp - 2 * 60)
                    )
                );

            $collection->load();

            if ($collection->count()) {
                foreach ($collection as $val) {
                    $leadRecord = new StdClass();
                    $leadRecord->Id = $val->getData("lead_id");
                    $leadAttributeList = new StdClass();
                    $attr = new StdClass();
                    $attr->attrName = "Magento_Inactivity_Time";
                    $attr->attrValue = ($timestamp - strtotime($val->getData("last_activity_time"))) / 60;
                    $leadAttributeList->attribute = $attr;
                    $leadRecord->leadAttributeList = $leadAttributeList;
                    $leadRecordList[] = $leadRecord;
                }
            }
        }

        if (!empty($leadRecordList)) {
            $leadModel->syncLead($leadRecordList, true);
        }
    }
}