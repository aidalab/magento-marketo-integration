<?php

class HooshMarketing_Marketo_Block_LeadAttributes extends Mage_Core_Block_Abstract implements Mage_Widget_Block_Interface
{
    protected function _toHtml()
    {
        $lead = Mage::getModel("hoosh_marketo/marketo");
        $leadData = $lead->getLead();

        $requiredTypes = $leadData->result->leadRecordList->leadRecord;
        $listAttr = $requiredTypes->leadAttributeList->attribute;

        $fieldName = $this->getData("fieldname");
        $default = $this->getData("default");
        /*$getParams = array(
            "FirstName",
            "LastName",
            "Email",
            "Score",
            "City",
            "PostCode",
            "Country",
            "Phone",
            "Address",
            "id",
            "MagentoSalesQuotes",
            "Company",
            "region_id"
        );*/
        /*foreach ($getParams as $val) {
            if ($val == $fieldName) {
                if ($fieldName == "Email" || $fieldName == "id") {
                    $name = $requiredTypes->$val;
                } else {
                    foreach ($listAttr as $attrib) {
                        if ($attrib->attrName == $val) {
                            $name = $attrib->attrValue;
                        }
                    }
                }
            }
        }*/
        $value = $default;

        if ($fieldName == "Email" || $fieldName == "id") {
            $value = $requiredTypes->$fieldName;
        } else {
            foreach ($listAttr as $attrib) {
                if ($attrib->attrName == $fieldName) {
                    $value = $attrib->attrValue;
                }
            }
        }

        return $value;
    }
}
