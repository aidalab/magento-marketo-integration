<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Админ
 * Date: 06.05.14
 * Time: 13:49
 * To change this template use File | Settings | File Templates.
 */
class HooshMarketing_Marketo_indexController extends Mage_Core_Controller_Front_Action
{

    public function indexAction()
    {

        $firstnameValue = $this->getRequest()->getPost("firstname");
        $lastnameValue = $this->getRequest()->getPost("lastname");
        $email = $this->getRequest()->getPost("email");
        $marketoAPI = Mage::getModel('hoosh_marketo/marketo');
        $confgiration = Mage::helper('hoosh_marketo')->getConf();
        $marketoAPI->__construct($confgiration->SoapEndpoint,$confgiration->Namespace, $confgiration->UserId, $confgiration->SecretKey,
                                 $confgiration->TimeZone, $confgiration->debugSoapResponse);
        $newLead = $marketoAPI->getLead();
        $this->id = $newLead->result->leadRecordList->leadRecord->Id;
        $options = new stdClass();
        $options->leadAttributeList = array();
        if (!empty($firstnameValue)||$firstnameValue!="")
        {
            $name = new  stdClass();
            $name->attrName = "FirstName";
            $name->attrValue = $firstnameValue;
            $options->leadAttributeList[] = $name;
        }
        if (!empty($lastnameValue)||$lastnameValue!="")
        {
            $lastname = new stdClass();
            $lastname->attrName = "LastName";
            $lastname->attrValue = $lastnameValue;
            $options->leadAttributeList[] = $lastname;
        }
        if (!empty($email) || $email!="")
        {
            $options->Email = $email;
        }
        $options->Id = $this->id;
        $lead = $marketoAPI->syncLead($options, true);
    }

    public function syncLastProductAction()
    {

        $marketoAPI = Mage::getModel('hoosh_marketo/marketo');
        $confgiration = Mage::helper('hoosh_marketo')->getConf();
        $marketoAPI->__construct($confgiration->SoapEndpoint,$confgiration->Namespace, $confgiration->UserId, $confgiration->SecretKey,
                             $confgiration->TimeZone, $confgiration->debugSoapResponse);
        $newLead = $marketoAPI->getLead();
    }

    public function syncallqueryAction()
    {
        $model = Mage::getSingleton("hoosh_marketo/mobject");
        $model::getLeadId();
        $leadKey = new StdClass();
        $leadKey->keyType = "IDNUM";
        $leadKey->keyValue = $model::$id;
        $type = new StdClass();
        $type->activityType = "NewLead";
        $inclTypes = new StdClass();
        $inclTypes->includeTypes = $type;
        $activity = $model::$config->getLeadActivity($leadKey, $inclTypes);
        $response = $activity->leadActivityList->returnCount;
        if($response)
        {
            $model::syncLead();
            $model::getAllData();
            $model::init();
        }
    }

    public function createCache()
    {
        $session = Mage::getSingleton("core/session");
        $time = Mage::helper("hoosh_marketo")->getConf()->cacheTime;
        $model = Mage::getSingleton("hoosh_marketo/mobject");
        $model::getConfig();
        $data = array();
        $data["lead"] = $model::$config->getLead();
        $data["time"] = round(microtime(true) * 1000) + $time*1000;
        $session->setData(array("marketo"=>$data));
    }

    public function timeAction()
    {
         $session = Mage::getModel("core/session");
        $sessionData = $session->getData("marketo");
        if(empty($sessionData))
        {
           $this->createCache();
    }

        if($sessionData["time"]<round(microtime(true)*1000))
        {
            $sess =  $this->updateSession($sessionData["time"]);
            $data = $sessionData["lead"];
            $array = $sess["lead"]->leadActivityList->activityRecordList->activityRecord;
            $requiredTypes =  $data->result->leadRecordList->leadRecord;
            $listAttr = $requiredTypes->leadAttributeList->attribute;
           for($j=0; $j<count($array); $j++)
           {
              $leadActivityAttr = $array[$j]->activityAttributes->attribute;
              $recordType = $array[$j]->mktgAssetName;
               switch ($recordType)
               {
                   case "Id":
                       $requiredTypes->Id =$leadActivityAttr[3]->attrValue;
                       break;
                   case "Email Address":
                       $requiredTypes->Email = $leadActivityAttr[3]->attrValue;
                       break;
                   case "First Name":
                   {
                       for($i=0; $i<count($listAttr);$i++)
                       {
                           if ($listAttr[$i]->attrName=="FirstName")
                           {
                               $listAttr[$i]->attrValue=$leadActivityAttr[3]->attrValue;
                           }
                       }
                   }
                       break;
                   case "Last Name":
                   {
                       for($i=0; $i<count($listAttr);$i++)
                       {
                           if ($listAttr[$i]->attrName=="LastName")
                           {
                               $listAttr[$i]->attrValue= $leadActivityAttr[3]->attrValue;
                           }
                       }
                   }
                       break;
                   case "Company":
                   {
                       for($i=0; $i<count($listAttr);$i++)
                       {
                           if ($listAttr[$i]->attrName=="Company")
                           {
                               $listAttr[$i]->attrValue=$leadActivityAttr[3]->attrValue;
                           }
                       }
                   }
                       break;
                   case "Lead Score":
                       for($i=0; $i<count($listAttr);$i++)
                       {
                           if ($listAttr[$i]->attrName=="LeadScore")
                           {
                               $listAttr[$i]->attrValue=$leadActivityAttr[3]->attrValue;

                           }
                      }
               }
        }
            $time = Mage::helper("hoosh_marketo")->getConf()->cacheTime;
            $sessionData["lead"]=$data;
            $sessionData["time"] = round(microtime(true) * 1000) + $time*1000;
            $session->setData(array("marketo"=>$sessionData));

    } else {
            var_dump($sessionData["lead"]);
        }

    }

    public function updateSession($sess_time)
    {
       $model = Mage::getSingleton("hoosh_marketo/mobject");
        $model::syncLead();
        $leadKey = new StdClass();
        $leadKey->keyType = "IDNUM";
        $leadKey->keyValue = $model::$id;
        $type = new StdClass();
        $type->activityType = "ChangeDataValue";
        $inclTypes = new StdClass();
        $inclTypes->includeTypes = $type;
        $time = new DateTimeZone('America/Godthab');
        $time= new DateTime(date('Y-m-d H:i:s', $sess_time/1000-3600*2), $time );
        $date = new StdClass();
        $date->oldestCreatedAt =$time->format(DATE_W3C);
        $data=array();
        $activity = $model::$config->getLeadActivity($leadKey, $inclTypes, $date);
        $data["lead"]=$activity;
        $time = Mage::helper("hoosh_marketo")->getConf()->cacheTime;
        $data["time"]=round(microtime(true) * 1000) + $time*1000;
        return $data;
    }

    public function getAction()
    {
        $model = Mage::getSingleton("hoosh_marketo/mobject");
        $model::getConfig();
       var_dump($model::$config->getLead());
    }


}
