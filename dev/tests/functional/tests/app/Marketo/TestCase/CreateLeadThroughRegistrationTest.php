<?php
namespace Mage\Customer\Test\TestCase;

use Mage\Customer\Test\Fixture\Customer;
use Mage\Cms\Test\Page\CmsIndex;
use Mage\Customer\Test\Page\CustomerAccountCreate;
use Mage\Customer\Test\Page\CustomerAccountLogout;


class CreateLeadThroughRegistration extends RegisterCustomerFrontendEntityTest
{
    /**
     * @before
     */
    public function azazaz() {

    }
    /**
     * Injection pages.
     *
     * @param CustomerAccountCreate $customerAccountCreate
     * @param CmsIndex $cmsIndex
     * @param CustomerAccountLogout $customerAccountLogout
     * @return void
     */
    public function __inject(
        CustomerAccountCreate $customerAccountCreate,
        CmsIndex $cmsIndex,
        CustomerAccountLogout $customerAccountLogout
    ) {
        $this->customerAccountCreate = $customerAccountCreate;
        $this->cmsIndex = $cmsIndex;
        $this->customerAccountLogout = $customerAccountLogout;
    }

    public function test(Customer $customer) {

    }
}